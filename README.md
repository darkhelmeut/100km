# 100km

Projet fait en 10 minutes¹, sur un coin de bureau, sous licence AGPL

## À quoi ça sert ?
Tracer un cercle de 100km de rayon autour d'un point donné

## Limite connue
- Ça ne prend pas en compte les règles locales (plages…)

## Possibilités
- Faire une recherche avec le champ de recherche
- Avoir une zone de 100km au *clic*
- Faire des unions de plusieurs zones (zones communes)
- Avoir accès à la ville dans l'infobulle des marqueurs
- Se géolocaliser (nécessite HTTPS)

## Technologies
- [OpenStreetMap](https://www.openstreetmap.org/)
- [Nominatim](https://nominatim.openstreetmap.org/)
- [Leaflet](https://leafletjs.com/)
- [Leaflet Control Geocoder](https://github.com/perliedman/leaflet-control-geocoder)
- [France Geojson](https://github.com/gregoiredavid/france-geojson)
- [Turf](https://turfjs.org/)

¹: En réalité un peu plus, mais la v1 c'était vraiment rapide.
